var express = require('express');

var app = express();

var port = process.env.OPENSHIFT_NODEJS_PORT || 8080;  
var ip = process.env.OPENSHIFT_NODEJS_IP || "127.0.0.1";

app.use('/css', express.static(__dirname + '/css'));

app.get('/', function(req, res){
	res.sendFile(__dirname + '/index.html');
});

app.listen(port, ip);